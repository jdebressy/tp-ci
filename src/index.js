/**
 * @param {string} name
 * @param {("Australian shepherd"|"Poodle"|"Newfoundland")} breed
 * @param {boolean} isMale
 * @returns {Object} a new puppy
 */
function createDog(name, breed, isMale) {
	return {
		name,
		breed,
		isMale,
	};
}

/**
 * @param {number} x
 * @param {number} y
 * @returns {Object}
 */
function additionInfo(x, y) {
	return {
		x,
		y,
		yAbs: Math.abs(y),
		sum: x + y,
	};
}

/**
 * @param {number[]} xs
 * @returns {number[]} 
 */
function removeEvenNumbers(xs) {
	if (!xs) return [];
	return xs.filter(x => x % 2 == 1);
}

/**
 * @param {number} x 
 * @returns {number} 
 */
function square(x) {
	return x * x;
}

/**
 * @param {number} x 
 * @returns {number[]}
 */
function multiplicationTable(x) {
	const values = [];
	for (let i = 0; i < 11; ++i) {
		values.push(i * x);
	}
	return values;
}

/**
 * @param {number} max 
 */
function printFullTable(max) {
	for (let i = 0; i < max; ++i) {
		let values = multiplicationTable(i);
		console.log(values);
	}
}

console.log(createDog("Jed", "Australian shepherd", true));

console.log(additionInfo(2, 6));
console.log(additionInfo(-5, -10));

console.log(removeEvenNumbers([]));
console.log(removeEvenNumbers(null));
console.log(removeEvenNumbers([-1, 1, 2, 0, 3, 4, 12, 11]));

console.log(square(0));
console.log(square(2));
console.log(square(5));

console.log("Table de 5", multiplicationTable(5));
console.log("Table de 7", multiplicationTable(7));

console.log(printFullTable(10));

module.exports = {
	createDog,
	additionInfo,
	removeEvenNumbers,
	square,
	multiplicationTable,
	printFullTable,
};
